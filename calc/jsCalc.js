window.onload = function () {
    function Console() {
        var globalVar = "";
        var buttons = document.getElementsByTagName('button'); 
        for (var i = 0, il = buttons.length; i < il; i++) {  
          buttons[i].onclick = function () {
              var displayNow = this.dataset.value;
              var addStrToInput = function(str) { 
              var input = document.getElementsByClassName('calc_display')[0];
               var oldVal = input.value;
               var newVal = oldVal + str + '';
               input.value = newVal; 
              }
              function counting() {
                  var memoryInput = document.getElementsByClassName('calc_display')[0].value;
                  globalVar = document.getElementsByClassName('calc_display')[0].value;
                  var memoryOperation = displayNow;
                  console.log(memoryInput);
                  console.log(memoryOperation);
              }
              function resetDisplay() {
                  var zeroDisplay = document.getElementsByClassName('calc_display')[0].value = 0;
              }
              if (this.dataset.type === "number") {
                  addStrToInput (displayNow);
              }
              else {
                  counting();
                  resetDisplay();
                  if (globalVar) {
                      var sum = parseInt('globalVar') + parseInt('memoryInput');
                      console.log(sum);
                  }
                  else {
                     var memoryInput = document.getElementsByClassName('calc_display')[0].value;
                  }
              }
          }
        }
    }

    Console();
    window.TRACE_LEVEL_INFO = 1;
    window.TRACE_LEVEL_WARNING = 2;
    window.TRACE_LEVEL_ERROR = 3;
    window.trace = function (message, traceLevel) {
        switch (traceLevel) {
            case TRACE_LEVEL_ERROR:
                alert("ERROR: " + message)
                break
            case TRACE_LEVEL_WARNING:
                console.log ("WARNING: " + message)
                break
            case TRACE_LEVEL_INFO:
            default:
                console.log ("INFO: " + message)
        }
    }

}

