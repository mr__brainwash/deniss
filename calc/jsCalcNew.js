
function initCalc () {
/*
   Объъявляем глобальные переменные для последуещей работы:
*/
    var operationResult = undefined;
    var currentOperation = undefined;
    var result = undefined;
/*
   Константы
*/
    var TYPE_NUMBER = 'number';
    var TYPE_OPERATION = 'operation';
/*
   Функции
*/
    var addStrToInput = function (str, input) {
        var oldVal = input.value;
        if (oldVal === '0') {
            var newVal = str + "";
        }
        else {
            var newVal = oldVal + str + "";
        }
        input.value = newVal;
    }
    var swFunc = function (operation, val1, val2) {
        switch (operation) {
            case '+': 
                   return  parseInt(val1) + parseInt(val2);
                    break;
            case '-': 
                   return  parseInt(val1) - parseInt(val2);
                    break;
            case '*': 
                   return  parseInt(val1) * parseInt(val2);
                    break;
            case '/': 
                   return  parseInt(val1) / parseInt(val2);
                    break;
        }
        currentOperation = undefined;
        operationResult = undefined;
    }
/*
    Присваиваем переменную массиву кнопок
*/
    var buttons = document.getElementsByTagName('button');
// Перебираем цикл с кнопками
    for (var i = 0, il = buttons.length; i < il; i++) {
/*
   Задаем действия при нажатии на кнопку
*/
        buttons[i].onclick = function () {
// Объявляем переменные для сокращения кода
            var value = this.dataset.value;
            var type = this.dataset.type;
            var input = document.getElementsByClassName('calc_display')[0];
/*
   Проверки и условия для работы калькулятора
*/
// Условия для ввода цифр
    if (type === TYPE_NUMBER) {
        if (operationResult && currentOperation && !result && input.value) {
            input.value = 0;
            addStrToInput(value, input);
        }
        /* else if (result && input.value) {
         result = 0;
         input.value = 0;
         } */
        else {
            addStrToInput(value, input);
        }
    }
    else if (type === TYPE_OPERATION) {
        if (value === 'CE' && input.value) {
            input.value = 0;
        }
// Условия для записи 1-го аргумента и знака в память
        else if (!operationResult) {
            operationResult = input.value;
            currentOperation = value;
        }
// Условия если нажата операция 2-й раз
        else if (operationResult && currentOperation && input.value) {
            swFunc(currentOperation, operationResult, input.value);
        }
    }
    }
    }
}







