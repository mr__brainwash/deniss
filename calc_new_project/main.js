//Функция для стандартных математических операций
function x (operation, val1, val2) {
    switch (operation) {
        case '+':
            return console.log (parseFloat(val1) + parseFloat(val2));
            break;
        case '-':
            return console.log (parseFloat(val1) - parseFloat(val2));
            break;
        case '*':
            return console.log (parseFloat(val1) * parseFloat(val2));
            break;
        case '/':
            if (val2 === 0) {
            return console.log ('Dividing by a zero is impossible')
            }
            else {
                return console.log(parseFloat(val1) / parseFloat(val2));
                break;
            }
    }
}
// Переменная для перевода радианы в градусы
var transferToDegrees = Math.PI / 180;
//// Функция вычесления синуса
function sin (arg) {
        return console.log (Math.sin(arg * transferToDegrees));
}
// Функция вычесления косинуса
function cos (arg) {
    return console.log (Math.cos(arg * transferToDegrees));
}
// Функция вычесления тангенса
function tan (arg) {
    return console.log (Math.tan(arg * transferToDegrees));
}
//Функция извлечения квадратного корня
function sqrt (arg) {
    return console.log (Math.sqrt(arg));
}
//Функция факториала
function factorial (n) {
    if (parseInt (n) != n) {
        return console.log('Enter an integer');
    }
    else {
        if (n === 0 || n === 1) {
            return console.log('1');
        }
        else if (n > 1) {
             var product = 1;
             for(i = 1; i <= n; i++) {
                  product *= i;
             }
             return console.log (product);
        }
        else {
        return console.log ('Invalid input');
        }
    }
}
// Функия Пи
function Pi () {
    return console.log (Math.PI);
}
// Функции Е
function EXP (exp) {
    return console.log (exp + ',e+0');
}
function e (e) {
    return console.log (Math.exp (e));
}
//Функция X в степени Y
function xY (x, y) {
    return console.log (Math.pow (x, y));
}
//Функция натурального логарифма
function ln (ln) {
    return console.log (Math.log(ln));
}
//Функция десятичного логарифма
function log (log) {
    if (log > 0) {
        return console.log ((Math.log(log)) / (Math.log(10)));
    }
    else {
        return console.log ('Enter a number greater than 0')
    }
}


