window.onload = function () {

//Глобальная функция для работы калькулятора

    function initCalc() {

/*
   Объъявляем глобальные переменные для последуещей работы:
*/
        var operationResult = undefined;
        var currentOperation = undefined;
/*
  Константы
*/
        var TYPE_NUMBER = 'number';
        var TYPE_OPERATION = 'operation';
// Переменная для перевода радианы в градусы
        var transferToDegrees = Math.PI / 180;
/*
  Функции
*/

// Функция ввода цифр на дисплей
        var addStrToInput = function (str, input) {
            var oldVal = input.value;
            if (oldVal === '0') {
                var newVal = str + "";
            }
            else {
                var newVal = oldVal + str + "";
            }
            input.value = newVal;
        };

// Функция заполнения массива при совершении операции
        var getArray = function(arr, opornIndex, value) {
            var newArray = new Array();
            for (var j = 0; j < opornIndex - 1; j++) {
                newArray.push(arr[j]);
            }
            newArray.push(value);
            for (var k = opornIndex + 2; k < arr.length; k++) {
                newArray.push(arr[k]);
            }
            return newArray;
        }


/*
  Присваиваем переменную массиву кнопок
*/

        var buttons = document.getElementsByTagName('button');

// Перебираем цикл с кнопками

        for (var i = 0, il = buttons.length; i < il; i++) {
            /*
             Задаем действия при нажатии на кнопку
             */

            buttons[i].onclick = function () {

// Объявляем переменные для сокращения кода
                var value = this.dataset.value;
                var type = this.dataset.type;
                var input = document.getElementsByClassName('calc_display')[0];
// Набор функций для не основных математических операций

// Функция вычесления синуса
                function sin (arg) {
                    return Math.sin(arg * transferToDegrees);
                }
// Функция вычесления косинуса
                function cos (arg) {
                    return Math.cos(arg * transferToDegrees);
                }
// Функция вычесления тангенса
                function tan (arg) {
                    return Math.tan(arg * transferToDegrees);
                }
//Функция извлечения квадратного корня
                function sqrt (arg) {
                    return Math.sqrt(arg);
                }
//Функция факториала
                function factorial (n) {
                    if (parseInt (n) != n) {
                        return 'Enter an integer :';
                    }
                    else {
                        if (n === 0 || n === 1) {
                            return 1;
                        }
                        else if (n > 1) {
                            var product = 1;
                            for(i = 1; i <= n; i++) {
                                product *= i;
                            }
                            return product;
                        }
                        else {
                            return 'Invalid input';
                        }
                    }
                }

//Функция X в степени Y
                function xY (x, y) {
                    return Math.pow (x, y);
                }
//Функция натурального логарифма
                function ln (ln) {
                    return Math.log(ln);
                }
//Функция десятичного логарифма
                function log (log) {
                    if (log > 0) {
                        return ((Math.log(log)) / (Math.log(10)));
                    }
                    else {
                        return 'Enter a number greater than 0';
                    }
                }
/*
  Проверки и условия для работы калькулятора
*/

// Условия для ввода цифр

                if (type == TYPE_NUMBER) {
                    if (operationResult && currentOperation) {
                        addStrToInput(value, input);
                    }
                    else {
                        addStrToInput(value, input);
                    }
                }
                else if (type == TYPE_OPERATION) {
                    if (value == 'CE' && input.value) {
                        input.value = 0;
                        operationResult = undefined;
                        currentOperation = undefined;
                    }
                    else if (value == 'sin') {
                        input.value = sin(input.value);
                    }
                    else if (value == 'cos') {
                        input.value = cos(input.value);
                    }
                    else if (value == 'tan') {
                        input.value = tan(input.value);
                    }
                    else if (value == '&radic') {
                        input.value = sqrt(input.value);
                    }
                    else if (value == 'x!') {
                        input.value = factorial(input.value);
                    }
                    else if (value == 'pi') {
                        input.value *= Math.PI;
                    }
// Проблема с этим

                   else if (value == 'EXP') {
                        input.value += 'E';
                        return false;
                    }

                    else if (value == 'e') {
                        input.value *= Math.E;
                    }
                    else if (value == 'xY') {


                    }


// Условия для записи 1-го аргумента и знака в память
                    else if (value !== '=' && input.value) {
                        operationResult = input.value;
                        currentOperation = value;
                        input.value +=' ' + value + ' ';
                    }

// Условия если нажата операция 2-й раз
                    else if (value == '=') {
                       var sorted = input.value.split(" ");
                       function result (array) {
                           if (array.length == '0' || array.length == '1') {
                               return array;
                           }
                           else {
                               for (var i = 0; i < array.length; i++) {
                                   if (array[i] == '/') {
                                       var divide =  parseFloat(array[i - 1]) /  parseFloat(array[i + 1]);
                                       return result(getArray(array, i, divide));

                                   }
                                   else if (array[i] == '*') {
                                       var multiply =  parseFloat(array[i - 1]) *  parseFloat(array[i + 1]);
                                       return result(getArray(array, i, multiply));

                                   }
                               }
                               for (var i = 0; i < array.length; i++) {
                                   if (array[i] == '+') {
                                       var plus =  parseFloat(array[i - 1]) +  parseFloat(array[i + 1]);
                                       return result(getArray(array, i, plus));
                                   }
                                   else if (array[i] == '-') {
                                       var minus =  parseFloat(array[i - 1]) -  parseFloat(array[i + 1]);
                                       return result(getArray(array, i, minus));
                                   }
                               }
                               return array;
                               /*
                                for (var i = 0; i < array.length; i++) {
                                var newArray = new Array ();
                                if (array[i] == '/') {
                                var divide = array[i-1] / array[i+1];
                                for ( var j = 0; j < i-1; j++) {
                                newArray.push (array[j]);
                                }
                                newArray.push(divide);
                                for ( var k = i+2; k < array.length; k++) {
                                newArray.push (array[k]);
                                }
                                return result(newArray);
                                }
                                else if (array[i] == '*') {
                                var multiply = array[i-1] * array[i+1];
                                for ( var h = 0; h < i-1; h++) {
                                newArray.push (array[h]);
                                }
                                newArray.push(multiply);
                                for ( var g = i+2; g < array.length; g++) {
                                newArray.push (array[g]);
                                }
                                return result(newArray);
                                }
                                }*/
                           }
                       }
                       input.value = result (sorted)[0];
                    }
                    else if (operationResult && currentOperation) {
                        input.value += ' ' + value + ' ';
                        operationResult = input.value;
                        currentOperation = value;
                    }

                }
            }
        }
    }

    initCalc();

}
