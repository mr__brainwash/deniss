angular.module('app', [])

.controller('CalcCtrl', ['$scope', '$calculateService', '$filter', function ($scope, $calculateService, $filter) {
    console.log($filter('$floorFilter')(4));
     $scope.result = '';
     $scope.TYPE_OPERATION = 'op';
     $scope.TYPE_NUM = 'num';
     $scope.btnClick = function (type, value) {
         $scope.$apply(function () {
            $scope.result = $calculateService(type, value);
         });
     };
     $scope.factorial = { val: 'x!', type: $scope.TYPE_OPERATION};
     $scope.bktLeft = { val: '(', type: $scope.TYPE_OPERATION};
     $scope.bktRight = { val: ')', type: $scope.TYPE_OPERATION};
     $scope.percent = { val: '%', type: 'TYPE_OPERATION'};
     $scope.ce = { val: 'CE', type: 'TYPE_OPERATION'};
     
     $scope.inv = { val: 'Inv', type: 'TYPE_OPERATION'};
     $scope.sin = { val: 'sin', type: 'TYPE_OPERATION'};
     $scope.in = { val: 'In', type: 'TYPE_OPERATION'};
     $scope.seven = { val: '7', type: 'TYPE_NUM'};
     $scope.eight = { val: '8', type: 'TYPE_NUM'};
     $scope.nine = { val: '9', type: 'TYPE_NUM'};
     $scope.divide = { val: '&divide;', type: 'TYPE_OPERATION'}; // возможны ошибки
     
     $scope.pi = { val: '&pi;', type: 'TYPE_OPERATION'}; // возможны ошибки
     $scope.cos = { val: 'cos', type: 'TYPE_OPERATION'};
     $scope.log = { val: 'log', type: 'TYPE_OPERATION'};
     $scope.four = { val: '4', type: 'TYPE_NUM'};
     $scope.five = { val: '5', type: 'TYPE_NUM'};
     $scope.six = { val: '6', type: 'TYPE_NUM'};
     $scope.times = { val: '&times;', type: 'TYPE_OPERATION'}; // возможны ошибки
     
     $scope.e = { val: 'e', type: 'TYPE_OPERATION'};
     $scope.tan = { val: 'tan', type: 'TYPE_OPERATION'};
     $scope.radic = { val: '&radic;', type: 'TYPE_OPERATION'}; // возможны ошибки
     $scope.one = { val: '1', type: 'TYPE_NUM'};
     $scope.two = { val: '2', type: 'TYPE_NUM'};
     $scope.three = { val: '3', type: 'TYPE_NUM'};
     $scope.minus = { val: '-', type: 'TYPE_OPERATION'};
     
     $scope.ans = { val: 'Ans', type: 'TYPE_OPERATION'};
     $scope.exp = { val: 'EXP', type: 'TYPE_OPERATION'};
     $scope.xy = { val: 'x<sup>y</sup>', type: 'TYPE_OPERATION'}; // возможны ошибки
     $scope.zero = { val: '0', type: 'TYPE_NUM'};
     $scope.point = { val: '.', type: 'TYPE_OPERATION'};
     $scope.equally = { val: '=', type: 'TYPE_OPERATION'};
     $scope.plus = { val: '+', type: 'TYPE_OPERATION'};
     
}])



.factory('$calculateService', [
    function() {
        return function (typeOfButton, valueOfButton) {
            return 1;
        }
    }
])
.directive('calcaluteBtn', function () {
    var link = function(scope, element) {
        element.on('click', function () {
            scope.click()(scope.customerInfo.type, scope.customerInfo.val);
        })
    };
    return {
            restrict: 'E',  
            link: link,
            scope: {
                customerInfo: '=info',
                click: '&'
            },
            template: "<button>{{ customerInfo.val }}</button>"
    };
})

.filter ('$floorFilter', function () {
    return function (val) {
        if (typeof(val) == 'number') {
            return val.toFixed(5); // Возможно, лучше будет использовать Math.round
        }
        else {
           return 'Введите число';
        }
    }
})

