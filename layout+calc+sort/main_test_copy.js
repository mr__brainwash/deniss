window.onload = function () {

//Глобальная функция для работы калькулятора

    function initCalc() {

/*
   Объъявляем глобальные переменные для последуещей работы:
*/
        var operationResult = undefined;
        var currentOperation = undefined;
/*
  Константы
*/
        var TYPE_NUMBER = 'number';
        var TYPE_OPERATION = 'operation';
// Переменная для перевода радианы в градусы
        var transferToDegrees = Math.PI / 180;
/*
  Функции
*/

// Функция ввода цифр на дисплей
        var addStrToInput = function (str, input) {
            var oldVal = input.value;
            if (oldVal === '0') {
                var newVal = str + "";
            }
            else {
                var newVal = oldVal + str + "";
            }
            input.value = newVal;
        };

// Функция заполнения массива при совершении операции
        var getArray = function(arr, opornIndex, value) {
            var newArray = new Array();
             for (var j = 0; j < opornIndex - 1; j++) {
                newArray.push(arr[j]);
            }
            newArray.push(value);
            for (var k = opornIndex + 2; k < arr.length; k++) {
                newArray.push(arr[k]);
            }
            return newArray;
        }


/*
  Присваиваем переменную массиву кнопок
*/

        var buttons = document.getElementsByTagName('button');

// Перебираем цикл с кнопками

        for (var i = 0, il = buttons.length; i < il; i++) {
            /*
             Задаем действия при нажатии на кнопку
             */

            buttons[i].onclick = function () {

// Объявляем переменные для сокращения кода
                var value = this.dataset.value;
                var type = this.dataset.type;
                var input = document.getElementsByClassName('calc_display')[0];
// Набор функций для не основных математических операций

// Функция вычесления синуса
                function sin (arg) {
                    return Math.sin(arg * transferToDegrees);
                }
// Функция вычесления косинуса
                function cos (arg) {
                    return Math.cos(arg * transferToDegrees);
                }
// Функция вычесления тангенса
                function tan (arg) {
                    return Math.tan(arg * transferToDegrees);
                }
//Функция извлечения квадратного корня
                function sqrt (arg) {
                    return Math.sqrt(arg);
                }
//Функция факториала
                function factorial (n) {
                    if (parseInt (n) != n) {
                        return 'Enter an integer :';
                    }
                    else {
                        if (n === 0 || n === 1) {
                            return 1;
                        }
                        else if (n > 1) {
                            var product = 1;
                            for(i = 1; i <= n; i++) {
                                product *= i;
                            }
                            return product;
                        }
                        else {
                            return 'Invalid input';
                        }
                    }
                }

//Функция X в степени Y
                function xY (x, y) {
                    return Math.pow (x, y);
                }
//Функция натурального логарифма
                function ln (ln) {
                    return Math.log(ln);
                }
//Функция десятичного логарифма
                function log (log) {
                    if (log > 0) {
                        return ((Math.log(log)) / (Math.log(10)));
                    }
                    else {
                        return 'Enter a number greater than 0';
                    }
                }
/*
  Проверки и условия для работы калькулятора
*/

// Условия для ввода цифр

                if (type == TYPE_NUMBER) {
                    if (operationResult && currentOperation) {
                        addStrToInput(value, input);
                    }
                    else {
                        addStrToInput(value, input);
                    }
                }
                else if (type == TYPE_OPERATION) {
                    if (value == 'CE' && input.value) {
                        input.value = 0;
                        operationResult = undefined;
                        currentOperation = undefined;
                    }
                    else if (value == 'sin') {
                        input.value = sin(input.value);
                    }
                    else if (value == 'cos') {
                        input.value = cos(input.value);
                    }
                    else if (value == 'tan') {
                        input.value = tan(input.value);
                    }
                    else if (value == '&radic') {
                        input.value = sqrt(input.value);
                    }
                    else if (value == 'x!') {
                        input.value = factorial(input.value);
                    }
                    else if (value == 'pi') {
                        input.value *= Math.PI;
                    }
// Проблема с этим

                   else if (value == 'EXP') {
                        input.value += 'E';
                        return false;
                    }

                    else if (value == 'e') {
                        input.value *= Math.E;
                    }
                    else if (value == 'xY') {


                    }


// Условия для записи 1-го аргумента и знака в память
                    else if (value !== '=' && input.value) {
                        operationResult = input.value;
                        currentOperation = value;
                        input.value +=' ' + value + ' ';
                    }

// Условия если нажата операция 2-й раз
                    else if (value == '=') {
                       var sorted = input.value.split(" ");
                       function result (array) {
                           if (array.length == '0' || array.length == '1') {
                               return array;
                           }
                           else {
                               for (var i = 0; i < array.length; i++) {
                                   if (array[i] == '/') {
                                       var divide =  parseFloat(array[i - 1]) /  parseFloat(array[i + 1]);
                                       return result(getArray(array, i, divide));

                                   }
                                   else if (array[i] == '*') {
                                       var multiply =  parseFloat(array[i - 1]) *  parseFloat(array[i + 1]);
                                       return result(getArray(array, i, multiply));

                                   }
                               }
                               for (var i = 0; i < array.length; i++) {
                                   if (array[i] == '+') {
                                       var plus =  parseFloat(array[i - 1]) +  parseFloat(array[i + 1]);
                                       return result(getArray(array, i, plus));
                                   }
                                   else if (array[i] == '-') {
                                       var minus =  parseFloat(array[i - 1]) -  parseFloat(array[i + 1]);
                                       return result(getArray(array, i, minus));
                                   }
                               }
                               return array;
                               /*
                                for (var i = 0; i < array.length; i++) {
                                var newArray = new Array ();
                                if (array[i] == '/') {
                                var divide = array[i-1] / array[i+1];
                                for ( var j = 0; j < i-1; j++) {
                                newArray.push (array[j]);
                                }
                                newArray.push(divide);
                                for ( var k = i+2; k < array.length; k++) {
                                newArray.push (array[k]);
                                }
                                return result(newArray);
                                }
                                else if (array[i] == '*') {
                                var multiply = array[i-1] * array[i+1];
                                for ( var h = 0; h < i-1; h++) {
                                newArray.push (array[h]);
                                }
                                newArray.push(multiply);
                                for ( var g = i+2; g < array.length; g++) {
                                newArray.push (array[g]);
                                }
                                return result(newArray);
                                }
                                }*/
                           }
                       }
                       input.value = result (sorted)[0];
                    }
                    else if (operationResult && currentOperation) {
                        input.value += ' ' + value + ' ';
                        operationResult = input.value;
                        currentOperation = value;
                    }

                }
            }
        }
    }

    initCalc();

// Глобальная функция для работы с сортировкой

    function initSort() {
        /*
         Объявляем переменные
         */
        var elementCalc = document.getElementById('calc');
        var whtsCalc = document.getElementById('wrapper');
        var elementSort = document.getElementById('sort');
        var whtsSort = document.getElementById('wrapArray');

// Обработчик нажатия на калькулятор
        function visibilityCalc() {
            elementCalc.onclick = function () {
                if (whtsSort.style.display == "block") {
                    whtsSort.style.display = "none";
                    whtsCalc.style.display = "block";
                }
                else {
                    whtsCalc.style.display = "block";
                }
            }
        }

        visibilityCalc();
// Обработчик нажатия на сортировку
        function visibilitySort() {
            elementSort.onclick = function () {
                if (whtsCalc.style.display == "block") {
                    whtsCalc.style.display = "none";
                    whtsSort.style.display = "block";
                }
                else {
                    whtsSort.style.display = "block";
                }
            }
        }

        visibilitySort();

//Функция генерирует максимальное значение елемента массива
        function generationMax() {
            var maxNumber = Math.round(Math.random() * 200);
            return maxNumber;
        }

        var a = generationMax();

//Функция генерирует минимальное значение елемента массива
        function generationMin(maxVariable) {
            var minNumber = Math.round(Math.random() * 100);
            if (minNumber > maxVariable) {
                var temporalMemory = maxVariable;
                maxVariable = minNumber;
                minNumber = temporalMemory;
            }
            else if (minNumber === maxVariable) {
                minNumber = minNumber - 9;
                maxVariable = minNumber + 10;
            }
            return minNumber;
        }

        var b = generationMin(a);


// Генерация случайного числа в диапазоне
        function genRanNumb(gMin, gMax) {
            var randNumber = Math.floor(Math.random() * (gMax - gMin + 1)) + gMin;
            return randNumber;
        }

        var c = genRanNumb(b, a);

//Функция генерирует количетво элементов массива
        function fillingRandomArray(maxEleArr, minEleArr) {
            var numbOfArrayElem = maxEleArr - minEleArr;
            if (numbOfArrayElem > 0) {
                return numbOfArrayElem;
            }
            else if (numbOfArrayElem === 0) {
                numbOfArrayElem += 20;
            }
            else {
                numbOfArrayElem = -(numbOfArrayElem);
                return numbOfArrayElem;
            }
        }

        var d = fillingRandomArray(a, b);

//Функция заполняет массив со случайным количеством элементов случайными числами
        function creationOfArray(FRA, GRN) {
            var arr = new Array(FRA);
            for (var i = 0; i < arr.length; i++) {
                arr [i] = {value: GRN(b, a)};
                console.log(arr[i].value);
            }
            return arr;
        }

        var e = creationOfArray(d, genRanNumb);


//Функция сравнения значений свойства
        function comparator_value(arg1, arg2) {
            if (arg1.value > arg2.value) {
                return 1;
            }
            else if (arg1.value < arg2.value) {
                return -1;
            }
            else {
                return 0;
            }
        }

//Функция сортировки
        function bubbleSort(CA, comVal, flag) {
            var copyArr = CA.slice(0);
            var swapped;
            var ifVal = flag ? 1 : -1;
            do {
                swapped = false;
                for (var i = 0; i < copyArr.length - 1; i++) {
                    if (comVal(copyArr[i], copyArr[i + 1]) === ifVal) {
                        var temp = copyArr[i];
                        copyArr[i] = copyArr[i + 1];
                        copyArr[i + 1] = temp;
                        swapped = true;
                    }
                }
            } while (swapped);
            return copyArr;
        }

// Функция заносит данные в div
        function textInDiv() {
            var p1 = document.getElementById('p_1');
            p1.textContent = 'Максимальное значение массива: ' + a;
            var p2 = document.getElementById('p_2');
            p2.textContent = 'Минимальное значение массива: ' + b;
            var p3 = document.getElementById('p_3');
            var p3Text = "Неотсортированный массив: [";
                 var unsortedArr = creationOfArray(d, genRanNumb);
                 for (var j = 0, jl = unsortedArr.length; j < jl; j++) { 
                    if (j > 0) 
                {
                    p3Text += ', ' ;
                    p3Text += unsortedArr[j].value; 
                }
            else
                if (j == 0) {
                    p3Text += unsortedArr[j].value; 
                }
            }
                  p3.textContent = p3Text + ']';
            var p4 = document.getElementById('p_4');
            var p4Text = "Отсортированный массив: [";
                 var sortedArr = bubbleSort(e, comparator_value, true);
                 for (var i = 0, il = sortedArr.length; i < il; i++) { 
                    if (i > 0) 
                {
                    p4Text += ', ' ;
                    p4Text += sortedArr[i].value; 
                }
            else
                if (i == 0) {
                    p4Text += sortedArr[i].value; 
                }

            }
                  p4.textContent = p4Text + ']';
        }

        textInDiv();

    }

    initSort();

    /*
     Функция для выпадания меню
     */
    function initMenu() {
        function util() {
            var utiliti = document.getElementById('utiliti');
            var calcConv = document.getElementById('menu1');
            utiliti.onclick = function () {
                if (calcConv.style.display == "block") {
                    calcConv.style.display = "none";

                }
                else {
                    calcConv.style.display = "block";
                }
            }
        }

        util();
        function different() {
            var different = document.getElementById('different');
            var sort = document.getElementById('sort');
            different.onclick = function () {
                if (sort.style.display == "block") {
                    sort.style.display = "none";
                }
                else {
                    sort.style.display = "block";
                }
            }
        }

        different();
        function moveMenu () {
            var buttonMenu = document.getElementById('gorizLines');
            var mainContainer = document.getElementById('something-semantic');
            buttonMenu.onclick = function () {
                if (mainContainer.classList.contains('closeSideMenu')){
                    mainContainer.classList.remove('closeSideMenu');
                }
                else {
                    mainContainer.className = 'closeSideMenu';
                }
            }
        }
        moveMenu ();
    }
    initMenu();

}
