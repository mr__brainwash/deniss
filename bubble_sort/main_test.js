window.onload = function () {

//Глобальная функция для работы калькулятора

    function initCalc() {
/*
   Объъявляем глобальные переменные для последуещей работы:
*/
        var operationResult = undefined;
        var currentOperation = undefined;
        var result = undefined;
        /*
         Константы
         */
        var TYPE_NUMBER = 'number';
        var TYPE_OPERATION = 'operation';

/*
  Функции
*/

// Функция ввода цифр на дисплей
        var addStrToInput = function (str, input) {
            var oldVal = input.value;
            if (oldVal === '0') {
                var newVal = str + "";
            }
            else {
                var newVal = oldVal + str + "";
            }
            input.value = newVal;
        };

// Функция подсчета основных мат. операций
        var swFunc = function (operation, val1, val2) {
            switch (operation) {
                case '+': 
                   return  parseInt(val1) + parseInt(val2);
                    break;
                case '-': 
                   return  parseInt(val1) - parseInt(val2);
                    break;
                case '*': 
                   return  parseInt(val1) * parseInt(val2);
                    break;
                case '/': 
                   return  parseInt(val1) / parseInt(val2);
                    break;
            }
            currentOperation = undefined;
            operationResult = undefined;
        };

/*
  Присваиваем переменную массиву кнопок
*/

        var buttons = document.getElementsByTagName('button');

// Перебираем цикл с кнопками

        for (var i = 0, il = buttons.length; i < il; i++) {
/*
   Задаем действия при нажатии на кнопку
*/

            buttons[i].onclick = function () {

// Объявляем переменные для сокращения кода
                var value = this.dataset.value;
                var type = this.dataset.type;
                var input = document.getElementsByClassName('calc_display')[0];
/*
   Проверки и условия для работы калькулятора
*/

// Условия для ввода цифр
                if (type === TYPE_NUMBER) {
                    if (operationResult && currentOperation && !result && input.value) {
                        input.value = 0;
                        addStrToInput(value, input);
                    }
                    else {
                        addStrToInput(value, input);
                    }
                }
                else if (type === TYPE_OPERATION) {
                    if (value === 'CE' && input.value) {
                        input.value = 0;
                    }

// Условия для записи 1-го аргумента и знака в память
                    else if (!operationResult) {
                        operationResult = input.value;
                        currentOperation = value;
                    }

// Условия если нажата операция 2-й раз
                    else if (operationResult && currentOperation && input.value) {
                        swFunc(currentOperation, operationResult, input.value);
                    }
                }
            }
        }
    }
    initCalc ();

// Глобальная функция для работы с сортировкой

    function initSort() {
/*
   Объявляем переменные
*/
        var elementCalc = document.getElementById('calc');
        var whtsCalc = document.getElementById('wrapper');
        var elementSort = document.getElementById('sort');
        var whtsSort = document.getElementById('wrapArray');

// Обработчик нажатия на калькулятор
        function visibilityCalc() {
            elementCalc.onclick = function () {
                if (whtsSort.style.display == "block") {
                    whtsSort.style.display = "none";
                    whtsCalc.style.display = "block";
                }
                else {
                    whtsCalc.style.display = "block";
                }
            }
        }
        visibilityCalc();
// Обработчик нажатия на сортировку
        function visibilitySort() {
            elementSort.onclick = function () {
                if (whtsCalc.style.display == "block") {
                    whtsCalc.style.display = "none";
                    whtsSort.style.display = "block";
                }
                else {
                    whtsSort.style.display = "block";
                }
            }
        }
        visibilitySort();

//Функция генерирует максимальное значение елемента массива
        function generationMax() {
            var maxNumber = Math.round(Math.random() * 200);
            return maxNumber;
        }

        var a = generationMax();

//Функция генерирует минимальное значение елемента массива
        function generationMin(maxVariable) {
            var minNumber = Math.round(Math.random() * 100);
            if (minNumber > maxVariable) {
                var temporalMemory = maxVariable;
                maxVariable = minNumber;
                minNumber = temporalMemory;
            }
            else if (minNumber === maxVariable) {
                minNumber = minNumber - 9;
                maxVariable = minNumber + 10;
            }
            return minNumber;
        }

        var b = generationMin(a);


// Генерация случайного числа в диапазоне
        function genRanNumb(gMin, gMax) {
            var randNumber = Math.floor(Math.random() * (gMax - gMin + 1)) + gMin;
            return randNumber;
        }

        var c = genRanNumb(b, a);

//Функция генерирует количетво элементов массива
        function fillingRandomArray(maxEleArr, minEleArr) {
            var numbOfArrayElem = maxEleArr - minEleArr;
            if (numbOfArrayElem > 0) {
                return numbOfArrayElem;
            }
            else if (numbOfArrayElem === 0) {
                numbOfArrayElem += 20;
            }
            else {
                numbOfArrayElem = -(numbOfArrayElem);
                return numbOfArrayElem;
            }
        }

        var d = fillingRandomArray(a, b);

//Функция заполняет массив со случайным количеством элементов случайными числами
        function creationOfArray(FRA, GRN) {
            var arr = new Array(FRA);
            for (var i = 0; i < arr.length; i++) {
                arr [i] = {value: GRN(b, a)};
                console.log(arr[i].value);
            }
            return arr;
        }
        var e = creationOfArray(d, genRanNumb);


//Функция сравнения значений свойства
            function comparator_value(arg1, arg2) {
                if (arg1.value > arg2.value) {
                    return 1;
                }
                else if (arg1.value < arg2.value) {
                    return -1;
                }
                else {
                    return 0;
                }
            }

//Функция сортировки
            function bubbleSort(CA, comVal, flag) {
                var copyArr = CA.slice(0);
                var swapped;
                var ifVal = flag ? 1 : -1;
                do {
                    swapped = false;
                    for (var i = 0; i < copyArr.length - 1; i++) {
                        if (comVal(copyArr[i], copyArr[i + 1]) === ifVal) {
                            var temp = copyArr[i];
                            copyArr[i] = copyArr[i + 1];
                            copyArr[i + 1] = temp;
                            swapped = true;
                        }
                    }
                } while (swapped);
                return copyArr;
            }

// Функция заносит данные в div
            function textInDiv() {
                var p1 = document.getElementById('p_1');
                p1.textContent = 'Максимальное значение массива: ' + a;
                var p2 = document.getElementById('p_2');
                p2.textContent = 'Минимальное значение массива: ' + b;
                var p3 = document.getElementById('p_3');
                var p3Text = "Неотсортированный массив: [";
                 var unsortedArr = creationOfArray(d, genRanNumb);
                 for(var j = 0, jl = unsortedArr.length; j < jl; j++) { 
                    if (j > 0) 
                    {
                        p3Text += ', ' ;
                        p3Text += unsortedArr[j].value; 
                    }
                else if (j == 0) {
                        p3Text += unsortedArr[j].value; 
                    }
                }
                  p3.textContent = p3Text + ']';
                var p4 = document.getElementById('p_4');
                var p4Text = "Отсортированный массив: [";
                 var sortedArr = bubbleSort(e, comparator_value, true);
                 for(var i = 0, il = sortedArr.length; i < il; i++) { 
                    if (i > 0) 
                    {
                        p4Text += ', ' ;
                        p4Text += sortedArr[i].value; 
                    }
                    else if (i == 0) {
                        p4Text += sortedArr[i].value; 
                    }

                }
                  p4.textContent = p4Text + ']';
            }

            textInDiv();

     }
    initSort ();

/*
    Функция для выпадания меню
 */
    function initMenu () {
        function util () {
            var utiliti = document.getElementById ('utiliti');
            var calcConv = document.getElementById('menu1');
            utiliti.onclick = function () {
                if (calcConv.style.display == "block") {
                    calcConv.style.display = "none";

                }
                else {
                    calcConv.style.display = "block";
                }
            }
         }
        util ();
        function different () {
            var different = document.getElementById ('different');
            var sort = document.getElementById('sort');
            different.onclick = function () {
                if (sort.style.display == "block") {
                    sort.style.display = "none";

                }
                else {
                    sort.style.display = "block";
                }
            }
        }
        different ();
        function moveContent () {
            var button = document.getElementById ('')
        }
        function clearLeft () {
            var left = document.getElementById ('left');

        }
    }
    initMenu();
}
