//Функция генерирует максимальное значение елемента массива
function generationMax () {
    var maxNumber = Math.round(Math.random() * 200);
    console.log (' Max number: ' +  maxNumber);
    return maxNumber;
}
generationMax ();

// Задаем переменную для функции
var maxVariable = generationMax ();

//Функция генерирует минимальное значение елемента массива
function generationMin () {
    var minNumber = Math.round(Math.random() * 100);
    if (minNumber > maxVariable) {
        var temporalMemory = maxVariable;
        maxVariable = minNumber;
        minNumber = temporalMemory;
    }
    else if (minNumber === maxVariable) {
        minNumber = Math.sqr(minNumber - 9);
        maxVariable = Math.sqr(minNumber + 10);
    }
    return minNumber;
}
// Генерация случайного числа в диапазоне
function genRanNumb () {
    var randNumber = Math.floor(Math.random( ) * (maxVariable - generationMin () + 1)) + generationMin ();
    return randNumber;
}
genRanNumb ();

//Функция генерирует количетво элементов массива
function fillingRandomArray () {
    var numbOfArrayElem = maxVariable - generationMin ();
    return numbOfArrayElem;
}
fillingRandomArray ();

//Функция заполняет массив со случайным количеством элементов случайными числами
function creationOfArray () {
    var arr = new Array (fillingRandomArray());
    for (var i = 0; i < arr.length; i++) {
        arr [i] = genRanNumb ();
    }
    return arr;
}
creationOfArray ();
//Создаем переменную для массива arr
var copMassive = creationOfArray ();

//Функция сортировки
function bubbleSort() {
    var copyArr = copMassive.slice(0);
    var swapped;
    do {
        swapped = false;
        for (var i=0; i < copyArr.length-1; i++) {
            if (copyArr[i] > copyArr[i+1]) {
                var temp = copyArr[i];
                copyArr[i] = copyArr[i+1];
                copyArr[i+1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
    console.log (copyArr);
}
console.log (copMassive);
bubbleSort ();