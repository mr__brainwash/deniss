$(document).ready(function() {

    function util() {
        var utiliti = document.getElementById('utiliti');
        var calcConv = document.getElementById('menu1');
        utiliti.onclick = function () {
            if (calcConv.style.display == "block") {
                calcConv.style.display = "none";

            }
            else {
                calcConv.style.display = "block";
            }
        }
    }

    util();
    function different() {
        var different = document.getElementById('different');
        var sort = document.getElementById('menu2');
        different.onclick = function () {
            if (sort.style.display == "block") {
                sort.style.display = "none";
            }
            else {
                sort.style.display = "block";
            }
        }
    }

    different();
    function closeMenu() {
        var buttonMenu = $('#gorizLines');
        var mainContainer = $('#something-semantic');
        buttonMenu.on('click', function () {
            if (mainContainer.hasClass('closeSideMenu')) {
                mainContainer.removeClass('closeSideMenu');
            }
            else {
                mainContainer.addClass('closeSideMenu');
            }
        });
    }
    closeMenu ();


/*

  Отправка на данных на сервер

*/

    //Перехват отправки данных формы
    $('#forms .btn-success').on('click', function(){
        // проверяем пару полей
        login ();
        return false;
    });
    $('#exit').on('click', function () {
        logout ();
        return false;
    });
    function login () {
        var login = $('#forms [name="login"]').val();
        var password = $('#forms [name="password"]').val();
        $.ajax({
            type: 'POST',
            url: 'http://demo.headworks.com.ua/api/index.php',
            data: { action: 'login', login: login, password: password },
            success: function () {
                $('#enter').addClass('hidden');
                $('#exit').removeClass('hidden'); },
            error:  function(){
                alert('Возникла ошибка: неверный логин или пароль');
            }
        });
    }
    function logout () {
        $.ajax({
            type: 'POST',
            url: 'http://demo.headworks.com.ua/api/index.php?action=logout',
            data: {action: 'login', login: login, password: password },
            success: function () {
                $('#exit').removeClass('hidden');
                $('#enter').addClass('hidden'); },

            error: function(){
                alert('Возникла ошибка');
            }
        });
    }


/*
  Второе модальное окно с личной информацией
*/

    $('#datebirth').datepicker({});

    $('#formsPUD .btn-success').on('click', function(){
        // проверяем пару полей
        sendPUD ();
        return false;
    });
    function sendPUD () {
        var name = $('#formsPUD [name="name"]').val();
        var surname = $('#formsPUD [name="surname"]').val();
        var country = $('#formsPUD [name="country"]').val();
        var town = $('#formsPUD [name="town"]').val();
        var education = $('option').val();
        var dateOfBirth = $('#formsPUD [name="datebirth"]').val();
        $.ajax({
            type: 'POST',
            url: 'http://demo.headworks.com.ua/api/index.php',
            data: { action: 'doSomething', name: name, surname: surname, country: country, town: town, education: education, dateOfBirth: dateOfBirth},
            success: function () {
               },
            error:  function(){
                alert('Возникла ошибка: неверно введены данные пользователя');
            }
        });
    }


/*

    Функции для работы меню jQuery/Bootstrap

 */
    $('.helper').hide();
// Функции для работы пункта меню Bootstrap - Modal

    $('#buttonForJsModal').on('click', function () {
        $('#defaultModalForJs').modal('show');
    });
    $('#buttonForDKEM').on('click', function () {
        $('#DKEM').modal('show');
        $('#DKEM').modal({ keyboard: false });
    });

    $('.modalWindows .consoleOutput').on('shown.bs.modal', function (e) {
        console.log('shown.bs.modal');
    });
    $('.modalWindows .consoleOutput').on('show.bs.modal', function (e) {
        console.log('(show.bs.modal');
    });
    $('.modalWindows .consoleOutput').on('hide.bs.modal', function (e) {
        console.log('hide.bs.modal');
    });
    $('.modalWindows .consoleOutput').on('hidden.bs.modal', function (e) {
        console.log('hidden.bs.modal');
    });
    $('.modalWindows .consoleOutput').on('loaded.bs.modal', function (e) {
        console.log('loaded.bs.modal');
    });

// Функция для открытия/скрытия подпунктов меню

    $('.dropdown-main ul span li a').on('click', function () {
        var valuee = $(this).attr('data-value');
        var neededClass = '.' + valuee;
        $('.helper[class!=neededClass]').hide();
        $(neededClass).show();
    });
// Функции для работы пункта меню Bootstrap - Dropdown

// Функция в которой создаётся эмуляция select
    $('.transferToButton').on('click', function () {
        var tmp = $(this).html() + '';
        $(this).parent().parent().parent().find(':first-child').html(tmp);
    });

// Функции для работы кнопки, которая открывает первый dropdown
    $('#buttonForOpeningDropdown').on('click', function () {
        $('.JSdropdown').dropdown('toggle');
        return false;
    });

// Функции для работы ScrollSpy
   $('#buttonForRefreshScroll').on('click', function () {
            $('[data-spy="scroll"]').each(function () {
                var $spy = $(this).scrollspy('refresh')
            });
    });
    $('#navbar-example').on('activate.bs.scrollspy', function () {
        //console.log('activate.bs.scrollspy');
        return false;
    });
// Функции вывода событий в консоль TAB
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        console.log('shown.bs.tab');
    });
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        console.log('show.bs.tab');
    });

// Функции для работы Tooltip
    $("div[data-position='left']").tooltip({ placement: 'left' });
    $("div[data-position='right']").tooltip({ placement: 'right' });
    $("div[data-position='top']").tooltip({ placement: 'top' });
    $("div[data-position='bottom']").tooltip({ placement: 'bottom' });

    $('#buttonForTooltip').tooltip({trigger: 'click'});
    $('#buttonForTooltipFormingInJs').tooltip({trigger: 'click', title: 'This is tooltip forming in JS'});

// Функции для работы Popover
    $('#popoverButton').popover({trigger: 'click'});
    $('#popoverButtonWithJsForming').popover({trigger: 'click', title: 'This is popover forming in JS', content: 'Hello, i am a popover'});

// Функции для работы Alert
    $('#buttonForAlertRed, #redClose').on('click', function () {
       $('.defaultError').toggleClass('hidden');
        return false;
    });
    $('#buttonForAlertYellow, #yellowClose').on('click', function () {
        $('.defaultErrorYellow').toggleClass('hidden');
        return false;
    });
    $('.closeAlertButtonYes, .closeAlertButtonNo').on('click', function () {
        $(".defaultErrorYellow").toggleClass('hidden');
    });
    $('.alert').on('closed.bs.alert', function () {
        console.log('closed.bs.alert');
    });

// Функции для работы Collapse
    $('#openCollapseTwo').on('click', function () {
        $('#collapseTwo').collapse('toggle');
    });
    $('.panel-collapse').on('hidden.bs.collapse', function () {
        console.log('hidden.bs.collapse');
    });
    $('#selectIcons').selectpicker();

// Функции для работы Gliphicons
    var glyphArray = ['glyphicon glyphicon-asterisk', 'glyphicon glyphicon-plus', 'glyphicon glyphicon-euro', 'glyphicon glyphicon-minus', 'glyphicon glyphicon-cloud', 'glyphicon glyphicon-envelope', 'glyphicon glyphicon-pencil', 'glyphicon glyphicon-glass'];
    $('#generateIcon').on('click', function () {
            var maxLimit = glyphArray.length;
            var ran = Math.floor( Math.random( )*maxLimit);
            var neededClassOfIcon = glyphArray[ran];
            var currentClass = $('#ourIcon').attr('class');
            $('#ourIcon').toggleClass(currentClass).toggleClass(neededClassOfIcon);
    });

// Функции для манипуляции с DOM JQuery
    $('#spanInDiv').on('click', function () {
        $('.CONTAINER').empty();
        var randNumberOfSpan = Math.floor(Math.random() * 5);
        var classSpan ='.' + 'spann' + randNumberOfSpan;
        $(classSpan).clone().appendTo('.CONTAINER');
        return false;
    });

// Функции для JQuery-selectors
    $('.buttonForNewButton').on('click', function () {
       $('.Selectors').append('<button type="button" class="btn btn-danger newBut">А вот и новая кнопка</button>');
       console.log('Нажата кнопка для создания');
    });
    $(document).on('click','.newBut',function(){
        console.log('Нажата созданная кнопка');
    });

// Функции для JQuery-Events
    //Создали объект
   var EventObject = new Object ();
    //В нем создали массив
   EventObject.arrayForEvents = new Array ();
    //При нажатии на кнопку значение инпута заносится в переменную
    $('#btnAdd').on('click', function () {
        var inputdata = $('#eventsInput').val();
        EventObject.AddEventListener(function () {
            console.log('inputText: ' + inputdata);
        });
    });
    // Объявляем переменную для использование в addEventListener и removeEventListener
    // Метод для добавления обработчка
    EventObject.AddEventListener = function (func) {
        EventObject.arrayForEvents.push(func)
    };
    // Метод для удаления обработчка
    EventObject.RemoveEventListener = function (func) {
        EventObject.arrayForEvents.remove(func)
    };
    // Метод, который будет оповещать всех подписчиков
    EventObject.DoA = function () {
        for (i = 0; i<EventObject.arrayForEvents.length; i++) {
            if (typeof EventObject.arrayForEvents[i] == 'function') {
                EventObject.arrayForEvents[i] ();
            }
            else {}
        }
    };
    // Обработчик события нажатия на кнопку "А" , в котором вызывается метод DoA
    $('#btnA').on('click',  EventObject.DoA);
});


