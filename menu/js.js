window.onload = function() {
    var changeSelectedTab = function(tabName) {
        switch(tabName) {
            case '#general':
                document.getElementById('vkl1').checked = true;
                break;
            case '#additional':
                document.getElementById('vkl2').checked = true;
                break;
            case '#confirmation':
                document.getElementById('vkl3').checked = true;
                break;
            default:
                window.location.hash = 'general';
                document.getElementById('vkl1').checked = true;
                break;
        }
    };
    changeSelectedTab (window.location.hash);

    function switchFirstHash() {
        window.location.hash = 'general'
    };
    document.getElementById('vkl1').onclick = switchFirstHash;

    function switchSecondHash() {
        window.location.hash = 'additional'
    };
    document.getElementById('vkl2').onclick = switchSecondHash;

    function switchThirdHash() {
        window.location.hash = 'confirmation'
    };
    document.getElementById('vkl3').onclick = switchThirdHash;
}